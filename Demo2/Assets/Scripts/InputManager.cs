﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using static CharacterInput;

[RequireComponent(typeof(Rigidbody))]
public class InputManager : MonoBehaviour, IControlActions
{
    private CharacterInput characterInput;
    public float Acceleration = 2f;  // gia tốc
    public float MaxSpeed;   //vận tốc lớn nhất
    public float JumpForce;
    private Rigidbody rigidbody;
    private Vector2 directionMovement;
    private Transform camera;

    private float curSpeed;   // vận tốc hiện tại
    private bool onGround;
    private bool isJumping;
    private Vector3 movementDirection;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        camera = Camera.main.transform;
        characterInput = new CharacterInput();
        characterInput.Enable();
        characterInput.Control.SetCallbacks(this);
    }

    public void OnMovement(InputAction.CallbackContext context)
    {
        directionMovement = context.ReadValue<Vector2>();
    }
    public void OnJump(InputAction.CallbackContext context)
    {
        if(context.started)
        {
            Jump();
        }
        Debug.Log($"Jump {context.started}");
    }

    private void FixedUpdate()
    {
        Movement();
    }

    private void Movement()
    {
        // Lấy hướng x và z của camera
        Vector3 cameraForward = Vector3.Scale(camera.forward, new Vector3(1, 0, 1)).normalized;
        // tính toán hướng di chuyển
        movementDirection = (directionMovement.x * camera.right + directionMovement.y * cameraForward).normalized;
        if(directionMovement.magnitude > 0)
        {
            curSpeed += Acceleration * Time.fixedDeltaTime;
            curSpeed = Mathf.Clamp(curSpeed, 0, MaxSpeed);
            transform.forward = cameraForward;
        }
        else
        {
            curSpeed = 0;
        }

        rigidbody.velocity = movementDirection * curSpeed + new Vector3(0,rigidbody.velocity.y,0);
    }

    private void Jump()
    {
        Debug.Log($"Jump onGround {onGround} :: isJumping {isJumping}");
        if(onGround && !isJumping)
        {
            isJumping = true;
            rigidbody.AddForce(new Vector3(0, JumpForce, 0), ForceMode.VelocityChange);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!onGround)
        {
            if (((1 << other.gameObject.layer) & LayerMask.NameToLayer("Player")) != 0)
            {
                onGround = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(onGround)
        {
            if (((1 << other.gameObject.layer) & LayerMask.NameToLayer("Player")) != 0)
            {
                onGround = false;
                isJumping = false;
            }
        }      
    }

    private void OnTriggerStay(Collider other)
    {
        if (!onGround)
        {
            if (((1 << other.gameObject.layer) & LayerMask.NameToLayer("Player")) != 0)
            {
                onGround = true;
            }
        }  
    }
}
